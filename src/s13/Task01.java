package s13;

import java.time.LocalDate;

public class Task01 {
    public static void main(String[] args) throws IllegalAccessException {
        Driver driver = new Driver();
        driver.setFirstName("Anastasiya");
        driver.setPatronymic("Nikolaevna");
        driver.setLastName("Yakimova");
        driver.setDateBorn(LocalDate.of(1995, 5, 4));
        driver.setDateLicense(LocalDate.of(2022, 7, 4));
        driver.setNumberLicense("567654£££");

        Validator validator = new Validator();
        //validator.validate(driver);
        if (validator.validate(driver)) {
            System.out.println("Данные приняты!");
        } else {
            System.out.println("Данные не приняты!");
        }
    }
}
