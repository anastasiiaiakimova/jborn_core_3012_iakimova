package s13;

import java.time.LocalDate;

public class Driver {
    @Regexp(regexp = "[a-zA-Z]+")
    @NotNull
    @NotEmpty
    @Max(lenghtMax = 10)
    @Min(lenghtMin = 3)
    private String firstName;
    @Regexp(regexp = "[a-zA-Z]+")
    @NotNull
    @NotEmpty
    @Max(lenghtMax = 10)
    @Min(lenghtMin = 3)
    private String lastName;
    @Regexp(regexp = "[a-zA-Z]+")
    @NotNull
    @NotEmpty
    @Max(lenghtMax = 10)
    @Min(lenghtMin = 3)
    private String patronymic;
    @NotNull
    @NotEmpty
    private LocalDate dateBorn;
    @Regexp(regexp = "[\\d]+")
    @NotNull
    @NotEmpty
    @Max(lenghtMax = 10)
    @Min(lenghtMin = 3)
    private String numberLicense;
    @NotNull
    @NotEmpty
    private LocalDate dateLicense;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setDateBorn(LocalDate dateBorn) {
        this.dateBorn = dateBorn;
    }

    public void setNumberLicense(String numberLicense) {
        this.numberLicense = numberLicense;
    }

    public void setDateLicense(LocalDate dateLicense) {
        this.dateLicense = dateLicense;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public LocalDate getDateBorn() {
        return dateBorn;
    }

    public String getNumberLicense() {
        return numberLicense;
    }

    public LocalDate getDateLicense() {
        return dateLicense;
    }
}
