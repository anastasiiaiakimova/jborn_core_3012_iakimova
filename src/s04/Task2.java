package s04;

public class Task2 {
    public static void main(String[] args) {
        System.out.println(reversStr("Jborn"));
    }

    public static String reversStr(String strDef) {
        String firstPart;
        String secondPart;
        int lenStr = strDef.length();

        if (lenStr <= 1) {
            return strDef;
        }

        firstPart = strDef.substring(0, lenStr / 2);
        secondPart = strDef.substring(lenStr / 2, lenStr);

        return reversStr(secondPart) + reversStr(firstPart);
    }
}
