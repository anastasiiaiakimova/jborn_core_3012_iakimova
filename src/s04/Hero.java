package s04;

import java.util.Collections;

public class Hero {
    static int INITIAL_HEALTH = 100;

    protected int health;
    protected final int power;
    private final int speed;

    public Hero() {

        health = INITIAL_HEALTH;
        power = 50;
        speed = 50;
        //System.out.println("Hero");

    }

    public Hero(int power) {
        this.health = INITIAL_HEALTH;

        this.power = power % 100;
        this.speed = 100 - this.power;
    }


    public void printStatus() {
        System.out.println(
                "health \t" + repeatAsterisk(health / 10) + "\n" +
                        "power \t" + repeatAsterisk(power / 10) + "\n" +
                        "speed \t" + repeatAsterisk(speed / 10) + "\n"
        );
    }

    public void hit(Hero hero) {
        this.damage(hero.power);
    }

    public void damage(int power) {
        this.health -= power;
    }

    public static String repeatAsterisk(int times) {
        return String.join("", Collections.nCopies(times, "*"));
    }

    public int getHealth() {
        return health;
    }

    public int getPower() {
        return power;
    }

    public int getSpeed() {
        return speed;
    }
}