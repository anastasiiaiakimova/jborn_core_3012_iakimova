package s04;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        System.out.println("Введите номер числа Фибоначчи:");
        int n = requestNumber();
        System.out.println("Число номер " + n + " = " + fib(n));
    }

    static int fib(int n) {
        if (n == 0) {
            return 0;
        }

        if (n == 1) {
            return 1;
        }
        int res = 0;
        if (n > 1) {
            res = fib(n - 1) + fib(n - 2);
        } else if (n < 0) {
            res = fib(Math.abs(n)) * (int) (Math.pow(-1, (n + 1)));
        }

        return res;
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
