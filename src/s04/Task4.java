package s04;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Circle circle = new Circle();
        Circle circleSecond = new Circle();
        System.out.println("Площадь = " + circle.squareCircle());
        System.out.println("Перемещение центра окружности");
        System.out.println("Введите координаты центра окружности:");
        double x1 = requestNumber();
        circle.setXCentral(x1);
        double y1 = requestNumber();
        circle.setYCentral(y1);
        System.out.println("Введите новые координаты:");
        double x2 = requestNumber();
        double y2 = requestNumber();
        circle.changeCentre(x2, y2);
        System.out.println("Координаты центра:");
        System.out.println("x = " + circle.getXCentral());
        System.out.println("y = " + circle.getYCentral());

        System.out.println("Введите координаты центра второй окружности:");
        double xSecond = requestNumber();
        double ySecond = requestNumber();
        circleSecond.setXCentral(xSecond);
        circleSecond.setYCentral(ySecond);

        circle.generalPoint(circleSecond);

        if (circle.generalPoint(circleSecond)) {
            System.out.println("Окружности пересекаются!");
        } else {
            System.out.println("Окружности не пересекаются!");
        }
        System.out.println("Расстояние между центрами  =  "
                + circle.lengthCentre(circleSecond));

    }

    static double requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
