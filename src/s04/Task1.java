package s04;

public class Task1 {
    public static void main(String[] args) {
        Hero rox = new Hero(20);
        Hero nyx = new Hero(40);

        nyx.printStatus();
        System.out.println("Здоровье до удара = " + nyx.getHealth());
        System.out.println("Сила до удара = " + nyx.getPower());
        System.out.println("Скорость до удара = " + nyx.getSpeed());
        System.out.println();

        rox.hit(nyx); //удар

        nyx.printStatus();
        System.out.println("Здоровье после удара = " + nyx.getHealth());
        System.out.println("Сила после удара = " + nyx.getPower());
        System.out.println("Скорость после удара = " + nyx.getSpeed());



    }
}
