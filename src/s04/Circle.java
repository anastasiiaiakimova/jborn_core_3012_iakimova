package s04;

public class Circle {
    static int initialRadius = 8;

    private double r = initialRadius;
    private double s; // площадь круга
    private double xCentral; // координата центра по оси x
    private double yCentral; // координата центра по оси y¬


    public double squareCircle() {     //метод вычисления площади круга
        return Math.PI * Math.pow(this.r, 2);
    }

    public void changeCentre(double x2, double y2) {
        this.xCentral = x2;
        this.yCentral = y2;
    }

    public void setXCentral(double a) {
        this.xCentral = a;
    }

    public void setYCentral(double a) {
        this.yCentral = a;
    }

    public double getXCentral() {
        return xCentral;
    }

    public double getYCentral() {
        return yCentral;
    }

    //Метод нахождения расстояния между центрами
    public double lengthCentre(Circle circle) {
        return Math.sqrt(Math.pow((this.xCentral - circle.xCentral), 2)
                + Math.pow((this.yCentral - circle.yCentral), 2));
    }

    //Метод пересечения двух окружностей и расстояние между центрами
    public boolean generalPoint(Circle circle) {
        double f = this.lengthCentre(circle);
//        boolean flag;
//        if (f == 0) {
//            flag = true;
//        } else if (f > (this.r + circle.r)) {
//            flag = false;
//        } else if ((f == (this.r + circle.r)) || (f == (this.r - circle.r))) {
//            flag = true;
//        } else if (f > (this.r - circle.r)) {
//            flag = true;
//        } else {
//            flag = false;
//        }

        return !(f > (this.r + circle.r)) && ((f == 0) ||
                ((f == (this.r + circle.r))
                        || (f == (this.r - circle.r)))
                || (f > (this.r - circle.r)));
    }


}
