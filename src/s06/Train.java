package s06;

public class Train {
    Carriage[] carriage;

    public Train(Carriage[] carriage) {
        this.carriage = carriage;
    }

    public double calcGenMass() {
        double genMass = 0;
        for (int i = 0; i < this.carriage.length; i++) {
            genMass += this.carriage[i].getM();
        }
        return genMass;
    }

    public double calcSrMass() {
        return calcGenMass() / this.carriage.length;
    }
}
