package s06;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        String str1 = null;
        String str2 = null;
        String str3 = null;
        System.out.println("Введите строку:");
        String str = requestString();
        LowerCaseHandler lowerCaseHandler = new LowerCaseHandler();
        AlphabetHandler alphabetHandler = new AlphabetHandler();
        LongWordHandler longWordHandler = new LongWordHandler(3, 2);


        str1 = lowerCaseHandler.handleMessage(str);

        str2 = alphabetHandler.handleMessage(str1);

        str3 = longWordHandler.handleMessage(str2);

        if (str3 != null) {
            System.out.println(str3);
        } else {
            System.out.println("NullPointerException");
        }

    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
