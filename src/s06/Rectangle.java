package s06;

public class Rectangle implements Area {

    private final double sideWeight;
    private final double sideHeight;

    public Rectangle(double sideWeight, double sideHeight) {
        this.sideWeight = sideWeight;
        this.sideHeight = sideHeight;
    }


    public double calculateArea() {
        return sideWeight * sideHeight;
    }
}
