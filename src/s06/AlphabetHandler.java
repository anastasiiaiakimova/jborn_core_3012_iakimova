package s06;

import java.util.regex.Pattern;

public class AlphabetHandler implements Handler {

    @Override
    public String handleMessage(String message) {
        if (message == null) {
            return null;
        } else {
            String str;
            if (Pattern.matches("[а-яА-ЯёЁ a-zA-Z]+", message)) str = message;
            else {
                str = null;
            }
            return str;
        }
    }
}
