package s06;

import java.util.StringTokenizer;

public class LongWordHandler implements Handler {
    private int m;
    private int n;

    public LongWordHandler(int m, int n) {
        this.m = m;
        this.n = n;
    }

    @Override
    public String handleMessage(String message) {
        if (message == null) {
            return null;
        } else {
            String str = null;
            String[] words = message.split(" ");
            if (words.length >= m) {
                for (int i = 0; i < words.length; i++) {
                    if ((words[i].toCharArray().length) >= n) {
                        str = message;
                    } else {
                        str = null;
                        break;
                    }
                }
            }
            return str;
        }
    }
}
