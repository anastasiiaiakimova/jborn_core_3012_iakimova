package s06;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        System.out.println("Сколько вагонов в поезде?");
        int numCarriage = requestNumber();
        Carriage[] carriage = new Carriage[numCarriage];
        Train train = new Train(carriage);
        for (int i = 0; i < numCarriage; i++) {
            train.carriage[i] = new Carriage();
            System.out.println("Введите грузовместимость вагона " + (i + 1) + ":");
            train.carriage[i].setM(requestMass());
        }

        System.out.println("Общая грузовместимость = " + train.calcGenMass() +
                "\n" + "Средняя на вагон = " + train.calcSrMass());

    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static double requestMass() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
