package s06;

public class Triangle implements Area {
    private final double sideFirst;
    private final double sideSecond;
    private final double sideThird;


    public Triangle(double sideFirst, double sideSecond, double sideThird) {
        this.sideFirst = sideFirst;
        this.sideSecond = sideSecond;
        this.sideThird = sideThird;
    }


    public double calculateArea() {
        double areaCalc = 0;
        double p = ((sideFirst + sideSecond + sideThird) / 2);
        //проверка существования трегульника
        if ((sideFirst + sideSecond > sideThird) & (sideSecond + sideThird > sideFirst)
                & (sideFirst + sideThird > sideSecond)) {
            areaCalc = Math.sqrt(p * ((p - sideFirst) * (p - sideSecond) * (p - sideThird)));
        }
        return areaCalc;
    }
}
