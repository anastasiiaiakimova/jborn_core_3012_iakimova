package s06;

abstract class Shape {

    //метод вычисляющий периметр
    abstract double calculatePerimeter();

    //метод вычисляющий площадь
    abstract double calculateArea();
}
