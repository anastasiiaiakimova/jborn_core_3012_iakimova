package s06;

import java.sql.SQLOutput;

public class Task2 {
    public static void main(String[] args) {
        Area[] areas = new Area[3];
        areas[0] = new Rectangle(2.8, 3.7);
        areas[1] = new Square(2);
        areas[2] = new Triangle(1, 1, 8);
        System.out.println("Площадь прямоугольника = " + areas[0].calculateArea());
        System.out.println("Площадь квадрата = " + areas[1].calculateArea());
        System.out.println("Площадь треугольника = " + areas[2].calculateArea());
        System.out.println("Сумма площадей = " + sumAreas(areas));

    }

    public static double sumAreas(Area[] areas) {
        double sumCalc = 0;
        for (Area area : areas) {
            sumCalc += area.calculateArea();
        }
        return sumCalc;
    }
}
