package s16;

@FunctionalInterface
public interface Validator<T> {
    public boolean isValid(T obj);
}
