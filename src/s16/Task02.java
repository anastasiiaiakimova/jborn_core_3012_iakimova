package s16;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Task02 {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 10_000_000; i++) {
            integers.add((int) ((Math.random() * 200) - 100));
        }
        // System.out.println((integers));
        System.out.println("Последовательный поток - порядок всегда одинаковый ");
        long t0 = System.nanoTime();
//        for (Integer integer1 : integers.stream().filter(integer -> integer > 0).toList()) {
//            System.out.print(integer1 + " ");
//        }
        System.out.println(" Сумма " + integers.stream().filter(integer -> integer > 0).
                reduce(Integer::sum).orElse(0));
        long t1 = System.nanoTime();
        long millis = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
        System.out.printf("Время выполнения последовательным потоком: %d ms%n", millis);

        System.out.println("\nПараллельный поток - порядок элементов может быть различный, " +
                "но результат все равно одинаковый");
        long t2 = System.nanoTime();
//        for (Integer integer1 : integers.parallelStream().filter(integer -> integer > 0).toList()) {
//            System.out.print(integer1 + " ");
//        }
        System.out.println(" Сумма " + integers.parallelStream().filter(integer -> integer > 0).
                reduce(Integer::sum).orElse(0));
        long t3 = System.nanoTime();
        long millis1 = TimeUnit.NANOSECONDS.toMillis(t3 - t2);
        System.out.printf("Время выполнения параллельным потоком: %d ms%n", millis1);

        System.out.println("Разница во времени выполнения " + millis + " - " + millis1 +
                " = " + (millis - millis1) + " ms");

    }
}
