package s08;

public class MaxException extends Exception {
    private String message;

    public MaxException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}

