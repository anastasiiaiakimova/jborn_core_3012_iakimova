package s08;

import java.math.BigDecimal;

import static java.lang.Long.MAX_VALUE;

public class Generator {
    //сумма больше Long.MAX_VALUE
    public void invalidSumm(BigDecimal summNumbers) throws MaxException {
        BigDecimal bigDecimal = new BigDecimal(MAX_VALUE);
        if (summNumbers.compareTo(bigDecimal) > 0) {
            throw new MaxException("Exception: Summa is wrong!" + " " + summNumbers);
        }
    }

    //неверный формат файла
    public void invalidFormat(String name) throws MaxException {
        String[] str = name.split("/");
        int n = str.length - 1;
        String[] str2 = str[n].split("\\.");
        if (!str2[1].equals("txt")) {
            throw new MaxException("Exception: Format is wrong!" + " " + str[n]);
        }
    }

    // неверный формат числа
    public void invalidFormatNum(String[] str) throws NumberFormatException {
        for (String str1 : str) {
            try {
                 Long.parseLong(str1);
            } catch (NumberFormatException numberFormatException) {
                throw new NumberFormatException("Exception: Format number is wrong!" + " " + str1);
            }
        }


    }

}