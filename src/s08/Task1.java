package s08;

import java.io.*;
import java.math.BigDecimal;

public class Task1 {
    public static void main(String[] args) throws MaxException, IOException {
        File fileFirst = new File("/Users/anastasiiaiakimova/Documents/java/File/file1.txt");
        File fileSecond = new File("/Users/anastasiiaiakimova/Documents/java/File/file2.txt");
        BigDecimal summNum = BigDecimal.valueOf(0);
        Generator generator = new Generator();

        try {
            String str = readFirstLine(fileFirst);
            String[] arrStr = str.split(" ");
            generator.invalidFormatNum(arrStr);
            for (String strFor : arrStr) {
                BigDecimal bigDecimal = new BigDecimal(strFor);
                summNum = summNum.add(bigDecimal);
            }

            generator.invalidSumm(summNum);
            generator.invalidFormat(fileFirst.getPath());

        } catch (MaxException maxException) {
            System.out.println(maxException);
            maxException.printStackTrace();

        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println(fileNotFoundException + " Ошибка! Файл не найден!");
            fileNotFoundException.printStackTrace();

        } catch (IOException ioException) {
            System.out.println(ioException + " Ошибка! К файлу нет доступа!");
            ioException.getStackTrace();
        }
        try {
            writeToFirst(fileSecond, String.valueOf(summNum));
            generator.invalidFormat(fileSecond.getPath());

        } catch (MaxException maxException1) {
            System.out.println(maxException1);
            maxException1.printStackTrace();

        } catch (IOException ioException1) {
            System.out.println(ioException1 + " Ошибка! К файлу нет доступа!");
            ioException1.printStackTrace();

        } finally {
            System.out.println("Операция завершена!");
        }

    }


    //Чтение из файла
    static String readFirstLine(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String first = reader.readLine();
        reader.close();
        stream.close();
        return first;
    }

    //Запись строки в файл
    static void writeToFirst(File file, String value) throws IOException {
        FileWriter writer = new FileWriter(file, false);
        writer.write(value);
        writer.flush();
        writer.close();
    }


}
