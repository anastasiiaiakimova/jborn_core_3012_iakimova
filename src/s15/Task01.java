package s15;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Введите количество элементов в очереди:");
        BlockingQueue<String> queue = new BlockingQueue<>(requestNumber());
        System.out.println("Введите количество Producer:");
        int producers = requestNumber();
        System.out.println("Введите количество Consumer:");
        int consumers = requestNumber();

        for (int i = 0; i < producers; i++) {
            new Thread(new Producer(queue)).start();
            Thread.sleep(1000);
        }
        for (int i = 0; i < consumers; i++) {
            new Thread(new Consumer(queue)).start();
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static class Producer implements Runnable {
        private final BlockingQueue<String> queue;

        Producer(BlockingQueue<String> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            System.out.println("[Producer] запущен");
            while (true) {
                try {
                    queue.put(produce());
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private String produce() {
            Message message = new Message();
            return message.getName();
        }
    }

    public static class Consumer implements Runnable {
        private final BlockingQueue<String> queue;

        Consumer(BlockingQueue<String> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            System.out.println("Consumer запущен");
            while (true) {
                try {
                    consume();
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private void consume() throws InterruptedException {
            String name = queue.take();
            System.out.println("Hello, " + name + "!");
        }
    }
}
