package s15;

import java.util.LinkedList;
import java.util.List;

public class BlockingQueue<T> {

    private final List<T> queue = new LinkedList<>();
    private int limit = 10;

    public BlockingQueue(int limit) {
        this.limit = limit;
    }

    public synchronized void put(T item) throws InterruptedException {
        System.out.println("[BlockingQueue] put пробуем положить:" + item);
        while (this.queue.size() == this.limit) {
            System.out.println("[BlockingQueue] put очередь заполнена, ждем");
            wait();
        }
        if (this.queue.size() == 0) {
            notifyAll();
        }
        this.queue.add(item);
    }

    public synchronized T take() throws InterruptedException {
        System.out.println("[BlockingQueue] пробуем взять:");
        while (this.queue.size() == 0) {
            System.out.println("[BlockingQueue] take очередь пустая, ждем");
            wait();
        }
        if (this.queue.size() == this.limit) {
            System.out.println("[BlockingQueue] take очередь заполнена");
            notifyAll();
        }
        T item = this.queue.remove(0);
        System.out.println("[BlockingQueue] взяли " + item);
        return item;
    }


}