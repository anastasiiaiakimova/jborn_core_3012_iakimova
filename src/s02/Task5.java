package s02;

import java.util.Scanner;
import java.util.Arrays;

import static java.lang.Math.pow;

public class Task5 {
    public static void main(String[] args) {
        int[] num = new int[3];
        num[0] = requestNumber();
        num[1] = requestNumber();
        num[2] = requestNumber();
        //сортировка массива по возрастанию
        Arrays.sort(num);

        if ((pow(num[0], 2) + pow(num[1], 2)) == pow(num[2], 2)){
            System.out.println("Эти числа являются Пифагоровой тройкой");
        } else {
            System.out.println("Эти числа не являются Пифагоровой тройкой");
        }

    }
    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
