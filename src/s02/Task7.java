package s02;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Task7 {
    public static void main(String[] args) {
        double x = requestNumber();
        double y;
        if (x > 0) {
            y = pow(Math.sin(x), 2);
        } else {
            y = 1 - 2 * (Math.sin(pow(x, 2)));
        }
        System.out.println(y);
    }

    static double requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
