package s02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int sec = requestNumber();
        //System.out.println(sec);
        int hours = sec / 3600; //количество часов
        System.out.println(hours);
        int min = (sec % 3600) / 60; //количество минут
        System.out.println(min);
        //int sec2 = (sec % 3600) % 60; //количество секунд
        int sec2 = (sec % 60);
        System.out.println(sec2);

    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
