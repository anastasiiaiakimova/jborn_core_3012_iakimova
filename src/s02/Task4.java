package s02;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Task4 {
    public static void main(String[] args) {
        int x1 = requestNumber();
        int y1 = requestNumber();
        int x2 = requestNumber();
        int y2 = requestNumber();
        double wayBetweenPoint = Math.sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
        System.out.println(wayBetweenPoint);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
