package s02;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int n = requestNumber();
        int k = n;
        int i = 0; //сумма цифр числа N
        while (n > 0) {
            i = i + (n % 10);
            n = n / 10;
        }
        System.out.println("Сумма цифр в числе " + k + " = " + i);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
