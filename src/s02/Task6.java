package s02;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Task6 {
    public static void main(String[] args) {
        int r = requestNumber();
        funcRadius(r);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите радиус:");
        return scanner.nextInt();
    }

    static void funcRadius(int n) {
        double s = (Math.PI * pow(n, 2));
        double l = (Math.PI * 2 * n);
        System.out.println("Площадь круга:");
        System.out.println(s);
        System.out.println("Длина окружности:");
        System.out.println(l);
    }
}
