package s02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int n = requestNumber(); //размер массива
        int[] arrNumbers = new int[n];
        System.out.println("Заполнение массива");
        for (int i = 0; i < n; i++) {
            arrNumbers[i] = requestNumber();
        }
        int sumNumbers = 0; //сумма четных
        int multiNumbers = 1; //произведение нечетных
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                sumNumbers = sumNumbers + arrNumbers[i];
            } else {
                multiNumbers = multiNumbers * arrNumbers[i];
            }
        }
        System.out.println("Сумма четных элементов массива = " + sumNumbers);
        System.out.println("Произведение нечетных элементов массива = " + multiNumbers);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
