package s07;

public interface Converter<S, T> {
    T convert(S source);
}