package s07;

import java.time.LocalDate;

public class Driver {
    private String firstName;
    private String lastName;
    private String patronymic;
    private LocalDate dateBorn;
    private String numberLicense;
    private LocalDate dateLicense;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setDateBorn(LocalDate dateBorn) {
        this.dateBorn = dateBorn;
    }

    public void setNumberLicense(String numberLicense) {
        this.numberLicense = numberLicense;
    }

    public void setDateLicense(LocalDate dateLicense) {
        this.dateLicense = dateLicense;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public LocalDate getDateBorn() {
        return dateBorn;
    }

    public String getNumberLicense() {
        return numberLicense;
    }

    public LocalDate getDateLicense() {
        return dateLicense;
    }
}
