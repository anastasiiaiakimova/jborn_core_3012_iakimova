package s07;

import java.time.LocalDate;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Driver driver = new Driver();
        DriverValidator driverValidator = new DriverValidator();
        System.out.println("Введите имя:");
        driver.setFirstName(requestString());
        System.out.println("Введите фамилию:");
        driver.setLastName(requestString());
        System.out.println("Введите отчество:");
        driver.setPatronymic(requestString());
        System.out.println("Введите дату рождения yyyy-mm-dd:");
        driver.setDateBorn(requestDate());
        System.out.println(driver.getDateBorn());
        System.out.println("Введите дату выдачи прав yyyy-mm-dd:");
        driver.setDateLicense(requestDate());
        System.out.println(driver.getDateLicense());
        System.out.println("Введите номер лицезии (не менее 8 цифр):");
        driver.setNumberLicense(requestString());

        if (driverValidator.isValid(driver)) {
            System.out.println("Некорректный ввод!");
        } else {
            System.out.println("Данные приняты!");
        }
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    static LocalDate requestDate() {
        Scanner scanner = new Scanner(System.in);
        return LocalDate.of(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
    }
}
