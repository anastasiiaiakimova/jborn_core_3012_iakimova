package s07;

public class TypesConverter implements Converter<Person, Employee> {

    @Override
    public Employee convert(Person source) {
        Employee employee = new Employee();
        employee.setFirstName(source.getFirstName());
        employee.setLastName(source.getLastName());
        employee.setAge(source.getAge());
        return employee;
    }
}
