package s07;

public interface Validator<T> {
    public boolean isValid(T obj);
}
