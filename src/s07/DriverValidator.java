package s07;

public class DriverValidator implements Validator<Driver> {

    @Override
    public boolean isValid(Driver driver) {
        //проверка длины имени
        return ((driver.getFirstName() == null || driver.getFirstName().length() < 3) ||

                // проверка длины фамилии
                (driver.getLastName() == null || driver.getLastName().length() < 3) ||

                // проверка длины отчества
                (driver.getPatronymic() == null || driver.getPatronymic().length() < 3) ||

                //проверка даты получения прав не раньше даты рождения
                (driver.getDateLicense().isBefore(driver.getDateBorn())) ||

                //проверка длины серии прав не меньше 8 цифр
                (driver.getNumberLicense().length() < 8));

    }

}

