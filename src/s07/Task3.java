package s07;

import java.math.BigDecimal;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        System.out.println("Введите первое число:");
        Integer numFirst = requestInt();
        System.out.println("Введите первое число:");
        Long numSecond = requestLong();
        Task3 task3 = new Task3();
        System.out.println("Результат сравнения = " + task3.<Integer, Long>compareNumbers(numFirst, numSecond));
    }

    static int requestInt() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static long requestLong() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLong();
    }

    public <T, U extends Number> int compareNumbers(T numFirst, U numSecond) {
        BigDecimal bigDecimalFirst = new BigDecimal(String.valueOf(numFirst));
        BigDecimal bigDecimalSecond = new BigDecimal(String.valueOf(numSecond));
        return bigDecimalFirst.compareTo(bigDecimalSecond);

    }
}
