package s07;

public class Task2 {
    public static void main(String[] args) {
        Person person = new Person("Inna", "Ivanova", 33, 166, 56);

        TypesConverter typesConverter = new TypesConverter();
        Employee employee = typesConverter.convert(person);
        System.out.println("Имя: " + employee.getFirstName() + "\n" + "Фамилия " + employee.getLastName() + "\n" +
                "Возраст: " + employee.getAge());
    }
}
