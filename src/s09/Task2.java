package s09;

import java.io.*;
import java.util.Scanner;


import static java.lang.Math.random;

public class Task2 {
    public static void main(String[] args) throws IOException {

        File file = new File("/Users/anastasiiaiakimova/Documents/java/File/numbers.txt");
        try (FileWriter fileWriter = new FileWriter(file, false)) {
            for (int i = 0; i < 200; i++) {
                if (i % 2 == 0) {
                    fileWriter.write(((int) (random() * (100 + 1))) + " ");
                } else {
                    fileWriter.write(((int) (random() * (-101))) + " ");
                }
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
             FileWriter fileWriterPos = new FileWriter("/Users/anastasiiaiakimova/Documents/java/File/numbers_positive.txt", false);
             FileWriter fileWriterNeg = new FileWriter("/Users/anastasiiaiakimova/Documents/java/File/numbers_negative.txt", false)) {
            String line = bufferedReader.readLine();
            String[] arrString = line.split(" ");
            for (String str : arrString) {
                if (Integer.parseInt(str) < 0) {
                    fileWriterNeg.write(str + " ");
                } else {
                    fileWriterPos.write(str + " ");
                }
            }
        } catch (IOException | NumberFormatException exception) {
            exception.printStackTrace();
        }
    }
}


