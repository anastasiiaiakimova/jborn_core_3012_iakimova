package s09;

import java.io.*;

public class User implements Externalizable {
    private String firstname;
    private String lastname;

    public User() {
    }


    public User(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(firstname);
        out.writeObject(lastname);
    }

    @Override
    public String toString() {
        return "User{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        firstname = (String) in.readObject();
        lastname = (String) in.readObject();
    }

}
