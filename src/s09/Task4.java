package s09;

import java.io.*;

public class Task4 {
    public static void main(String[] args) throws FileNotFoundException {
        User user = new User("Ivan", "Ivanov");
        System.out.println("Before: \n" + user);
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("Externals.out"))) {
            out.writeObject(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("Externals.out"))) {
            user = (User) in.readObject();
        } catch (IOException | ClassNotFoundException ioException) {
            ioException.printStackTrace();
        }
        System.out.println("After: \n" + user);
    }
}
