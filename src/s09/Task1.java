package s09;

import javax.xml.stream.FactoryConfigurationError;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class Task1 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/Users/anastasiiaiakimova/IdeaProjects/JBorn_Core_3012_Iakimova");
        ArrayList<Path> arrayList = findFile(path, "Task1.java");
        for (Path path1 : arrayList) {
            System.out.println(path1.getFileName());
        }
    }

    public static ArrayList<Path> findFile(Path path, String fileName) throws IOException {
        ArrayList<Path> paths = new ArrayList<>();
        try {
            DirectoryStream<Path> stream = Files.newDirectoryStream(path);
            for (Path path1 : stream) {
                if (path1.toFile().isDirectory()) {
                    paths.addAll(findFile(path1, fileName));
                } else if (path1.toFile().getName().equals(fileName)) {
                    paths.add(path1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paths;
    }
}
