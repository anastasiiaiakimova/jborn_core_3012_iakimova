package s09;

import java.io.*;
import java.nio.charset.Charset;

public class Task3 {
    public static void main(String[] args) throws IOException {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new byte[]{65, 66, 67})) {
            String str = readAsString(byteArrayInputStream, Charset.forName("UTF-8"));
            System.out.println(str);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        Reader reader = new InputStreamReader(inputStream, charset);
        String str = null;
        try (StringWriter stringWriter = new StringWriter()) {
            int n;
            while ((n = reader.read()) != -1) {
                stringWriter.write(n);
            }
            str = stringWriter.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
