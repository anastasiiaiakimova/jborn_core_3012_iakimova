package s11;

import java.util.ArrayList;
import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        int[] arr = {3, 5, 2, 7, 0, 1, 8, 6, 9, 4};
        System.out.println(Arrays.toString(arr));
        int left = 0;
        int right = arr.length - 1;
        sortArrayQuick(arr, left, right);
        System.out.println(Arrays.toString(arr));
    }

    public static void sortArrayQuick(int[] num, int left, int right) {
        int pv;//разрешающий элемент
        int lHold = left;//левая граница
        int rHold = right; //правая граница
        pv = num[left];

        while (left < right) {//пока границы не равны
            while ((num[right] >= pv) && (left < right)) {
                right--;
            }
            if (left != right) {
                num[left] = num[right];
                left++;
            }
            while ((num[left] <= pv) && (left < right)) {
                left++;
            }
            if (left != right) {
                num[right] = num[left];
                right--;
            }
        }
        num[left] = pv;
        pv = left;
        left = lHold;
        right = rHold;
        if (left < pv) {
            sortArrayQuick(num, left, (pv - 1));
        }
        if (right > pv) {
            sortArrayQuick(num, pv + 1, right);
        }

    }
}

