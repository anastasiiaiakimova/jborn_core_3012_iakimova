package s11;


//класс дерева
public class BinaryTree<T extends Comparable<T>> {
    private NodeTask2<T> root;

    public BinaryTree() {
        root = null;
    }

    public NodeTask2<T> getRoot() {
        return root;
    }

    public void addNode(T value) { //метод вставки нового узла дерева
        NodeTask2<T> nodeTask2 = new NodeTask2<>(); //новый узел
        nodeTask2.setValue(value); //вставка данных узла
        if (this.root == null) {
            this.root = nodeTask2; //  если дерево пустое
        } else {
            NodeTask2<T> currentNode = root; //начнем с первого узла
            NodeTask2<T> parentNode;
            while (true) {
                parentNode = currentNode;
                if (value == currentNode.getValue()) {
                    System.out.println("Узел " + value + " уже есть");
                    return;
                } else if ((value.compareTo(currentNode.getValue())) < 0) { //если пришел ужел меньше идем влево
                    currentNode = currentNode.getLeft();
                    if (currentNode == null) {
                        parentNode.setLeft(nodeTask2); //поставили в конец цепочки слева
                        return;
                    }
                } else { // если узел больше идем вправо
                    currentNode = currentNode.getRight();
                    if (currentNode == null) {
                        parentNode.setRight(nodeTask2);
                        return;
                    }
                }

            }

        }
    }

    public NodeTask2<T> findNode(T value) { // поиск элемента по значению
        NodeTask2<T> nodeTask2 = this.root;
        while (nodeTask2.getValue() != value) {
            if ((value.compareTo(nodeTask2.getValue())) < 0) {
                nodeTask2 = nodeTask2.getLeft();
            } else {
                nodeTask2 = nodeTask2.getRight();
            }
            if (nodeTask2 == null) {
                return null;
            }
        }
        return nodeTask2;
    }

    //метод удаления узла
    public boolean deleteNode(T value) {
        NodeTask2<T> currentNode = this.root;
        NodeTask2<T> parentNode = this.root;
        boolean isLeft = true;

        while (!(currentNode.getValue().compareTo(value) == 0)) {
            parentNode = currentNode;
            if ((value.compareTo(currentNode.getValue())) < 0) {
                isLeft = true;
                currentNode = currentNode.getLeft();
            } else {
                isLeft = false;
                currentNode = currentNode.getRight();
            }
            if (currentNode == null) {
                return false; //такого узла нет
            }
        }
        if (currentNode.getRight() == null && currentNode.getLeft() == null) {
            if (currentNode == this.root) {
                this.root = null;
            } else if (isLeft) {
                parentNode.setLeft(null);
            } else {
                parentNode.setRight(null);
            }
        } else if (currentNode.getRight() == null) {
            if (currentNode == this.root) {
                this.root = currentNode.getLeft();
            } else if (isLeft) {
                parentNode.setLeft(currentNode.getLeft());
            } else {
                parentNode.setRight(currentNode.getLeft());
            }
        } else if (currentNode.getLeft() == null) {
            if (currentNode == this.root) {
                this.root = currentNode.getRight();
            } else if (isLeft) {
                parentNode.setLeft(currentNode.getRight());
            } else {
                parentNode.setRight(currentNode.getRight());
            }
        } else {
            NodeTask2<T> heirNode = searchHeirNode(currentNode);
            if (currentNode == this.root) {
                this.root = heirNode;
            } else if (isLeft) {
                parentNode.setLeft(heirNode);
            } else {
                parentNode.setRight(heirNode);
            }
        }
        return true;
    }

    private NodeTask2<T> searchHeirNode(NodeTask2<T> nodeTask2) {
        NodeTask2<T> parentNode = nodeTask2;
        NodeTask2<T> heirNode = nodeTask2;
        NodeTask2<T> currentNode = nodeTask2.getRight();
        while (currentNode != null) {
            parentNode = heirNode;
            heirNode = currentNode;
            currentNode = currentNode.getLeft();
        }
        if (heirNode != nodeTask2.getRight()) {
            parentNode.setLeft(heirNode.getRight());
            heirNode.setRight(nodeTask2.getRight());
        }
        return heirNode;
    }

    //обход дерева inOrder
    public void inOrder(NodeTask2<T> localRoot) {
        if (localRoot != null) {
            inOrder(localRoot.getLeft());
            System.out.print(localRoot.getValue() + " ");
            inOrder(localRoot.getRight());
        }
    }

}





