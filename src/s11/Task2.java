package s11;

public class Task2 {
    public static void main(String[] args) {
        BinaryTree<Integer> binaryTree = new BinaryTree<>();

        binaryTree.addNode(66);
        binaryTree.addNode(8);
        binaryTree.addNode(5);
        binaryTree.addNode(16);
        binaryTree.addNode(2);
        binaryTree.addNode(9);
        binaryTree.addNode(7);
        binaryTree.addNode(4);
        binaryTree.addNode(10);
        binaryTree.addNode(3);
        binaryTree.addNode(1);

        binaryTree.inOrder(binaryTree.getRoot());

        boolean del = binaryTree.deleteNode(10);
        if (del) {
            System.out.println("\nЭлемент удален:");
        } else {
            System.out.println("\nТакого узла нет!");
        }
        binaryTree.inOrder(binaryTree.getRoot());
        System.out.println();
        binaryTree.findNode(16).printNode();
        binaryTree.inOrder(binaryTree.getRoot());

    }
}
