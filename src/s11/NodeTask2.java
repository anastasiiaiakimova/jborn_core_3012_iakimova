package s11;

//класс узла
public class NodeTask2<T extends Comparable<T>> {
    private NodeTask2 left;
    private NodeTask2 right;
    private T value;

    public void printNode() {
        System.out.println("Выбранный узел имееет значение: " + value);
    }

    public NodeTask2 getLeft() {
        return this.left;
    }

    public void setLeft(NodeTask2 left) {
        this.left = left;
    }

    public NodeTask2 getRight() {
        return this.right;
    }

    public void setRight(NodeTask2 right) {
        this.right = right;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "NodeTask2{" +
                "left=" + left +
                ", right=" + right +
                ", value=" + value +
                '}';
    }
}
