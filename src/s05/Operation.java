package s05;

abstract class Operation {
    int lastDigital;
    int beforeLastDigital;

    //класс алгебраической операции
    abstract int calculate(int leftOperand, int rightOperand);

    //класс возвращения предыдущего знаечния
    int returnPrevious() {
        return beforeLastDigital;
    }
}

