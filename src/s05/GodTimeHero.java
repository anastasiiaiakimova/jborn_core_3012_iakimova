package s05;

import org.w3c.dom.ls.LSOutput;
import s04.Hero;


public class GodTimeHero extends Hero {

    private int godMinutes;
    private final long timeFirst;

    public GodTimeHero() {
        this.timeFirst = System.currentTimeMillis();
        this.godMinutes = 0;
    }


    public void setGodMinutes(int a) {
        this.godMinutes = a;
    }

    public int getGodMinutes() {
        return godMinutes;
    }

    public long getTimeFirst() {
        return timeFirst;
    }


    @Override
    public void damage(int power) {
        long date2 = System.currentTimeMillis();
        if ((date2 - this.timeFirst) >= (long) this.godMinutes * 1000 * 60) {
            super.damage(power);
        }
    }
}

