package s05;

import s04.Hero;

import java.util.Date;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        /* Hero heroFirst = new Hero(); */
        int min = requestNumber();
        GodTimeHero heroGod = new GodTimeHero();
        heroGod.setGodMinutes(min);

        System.out.println(heroGod.getGodMinutes());
        System.out.println(heroGod.getTimeFirst());
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите время неуязвимости:");
        return scanner.nextInt();
    }
}
