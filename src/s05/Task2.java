package s05;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Addiction s = new Addiction();
        Multiplication m = new Multiplication();

        System.out.println("Введите 2 числа:");
        int a = requestNumber();
        int b = requestNumber();

        System.out.println("Сумма равна = " + s.calculate(a, b) + " Предыдущее =  " + s.returnPrevious());
        System.out.println("Произведение равно = " + m.calculate(a, b) + " Предыдущее =  " + s.returnPrevious());

        System.out.println("Введите 2 числа:");
        int c = requestNumber();
        int d = requestNumber();

        System.out.println("Сумма равна = " + s.calculate(c, d) + " Предыдущее =  " + s.returnPrevious());
        System.out.println("Произведение равно = " + m.calculate(c, d) + " Предыдущее =  " + m.returnPrevious());


    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
