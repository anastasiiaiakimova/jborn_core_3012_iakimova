package s05;

public class Multiplication extends Operation {

    @Override
    int calculate(int leftOperand, int rightOperand) {
        beforeLastDigital = lastDigital;
        lastDigital = leftOperand * rightOperand;
        return lastDigital;
    }

}
