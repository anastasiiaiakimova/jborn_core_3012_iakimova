package s05;

public class Addiction extends Operation {


    @Override
    public int calculate(int leftOperand, int rightOperand) {
        beforeLastDigital = lastDigital;
        lastDigital = leftOperand + rightOperand;
        return lastDigital;
    }

}
