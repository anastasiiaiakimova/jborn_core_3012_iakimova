package s10;

public interface Queue<T>{
    boolean add(T e);  //добавить элемен в конец очереди
    T poll(); //удаляет верхний элемент очереди
    T peek(); //показывает верхний элемент очереди
}
