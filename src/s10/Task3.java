package s10;

import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {
        NodeTask3<Integer> first = new NodeTask3<>(1);
        NodeTask3<Integer> second = new NodeTask3<>(2);
        NodeTask3<Integer> third = new NodeTask3<>(3);
        NodeTask3<Integer> four = new NodeTask3<>(4);
        NodeTask3<Integer> five = new NodeTask3<>(5);
        NodeTask3<Integer> six = new NodeTask3<>(6);
        NodeTask3<Integer> seven = new NodeTask3<>(7);
        NodeTask3<Integer> eight = new NodeTask3<>(8);

        first.next = second;
        second.next = third;
        third.next = four;
        four.next = five;
        five.next = six;
        six.next = second;
        seven.next = eight;
        eight.next = null;

        QueueImpl<Object> queue = new QueueImpl<>(8);

        for (NodeTask3<Integer> integerNodeTask2 : Arrays.asList(first, second, third, four, five, six, seven, eight)) {
            queue.add(integerNodeTask2.element);
        }

        System.out.println("peek: " + queue.peek() + "\n");

        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());
        System.out.println("pool: " + queue.poll());


    }
}
