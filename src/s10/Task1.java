package s10;

public class Task1 {
    public static void main(String[] args) {
        Node first = new Node(1);
        Node second = new Node(2);
        Node third = new Node(3);
        Node four = new Node(4);
        Node five = new Node(5);
        Node six = new Node(6);
        Node seven = new Node(7);
        Node eight = new Node(8);

        first.next = second;
        second.next = third;
        third.next = four;
        four.next = five;
        five.next = six;
        six.next = second;
        seven.next = eight;
        eight.next = null;

        if (hasCycle(first)) {
            System.out.println("Список зациклен!");
        } else {
            System.out.println("Список не зациклен!");
        }
    }

    public static boolean hasCycle(Node first) {
        Node turtle = first;
        Node hare = first;

        while (hare != null && hare.next != null) {
            turtle = turtle.next;
            hare = hare.next.next;
            if (hare == turtle) {
                return true;
            }
        }
        return false;
    }
}
