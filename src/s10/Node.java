package s10;

public class Node<T> {
    T data;
    Node next;

    public Node(T i) {
        this.data = i;
    }
}
