package s10;

public class ListImpl<T> implements List<T> {
    private NodeTask2<T> head;
    private NodeTask2<T> last;

    private NodeTask2<T> searchNode(int index) {
        NodeTask2<T> current = this.head;
        if (index <= (lenght() / 2)) {
            for (int i = 0; i < index; i++) {
                current = current.getNext();
            }
        } else {
            for (int i = lenght(); i >= index; i--) {
                current = current.getNext();
            }
        }
        return current;
    }


    @Override
    public T get(int index) {
        if (index >= lenght() || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + lenght());
        }
        NodeTask2<T> single = searchNode(index);
//        NodeTask2<T> single = this.head;
//        for (int i = 0; i < index; i++) {
//            single = single.next;
//        }
        return single.data;
    }

    @Override
    public void put(T e) {
        NodeTask2<T> elemAdd = new NodeTask2<>(e);
        if (this.head == null) {
            this.head = elemAdd;
            this.last = elemAdd;
        } else {
            last.setNext(elemAdd);
            elemAdd.setPrev(last);
            this.last = elemAdd;
        }
    }

    @Override
    public void put(int index, T e) {
        if (index >= lenght() || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + lenght());
        }
        NodeTask2<T> nodeTask2 = new NodeTask2<>(e);

        NodeTask2<T> cur = searchNode(index);
//        for (int i = 0; i < index; i++) {
//            cur = cur.getNext();
//        }
        nodeTask2.setNext(cur);
        nodeTask2.setPrev(cur.getPrev());
        nodeTask2.getPrev().setNext(nodeTask2);
        cur.setPrev(nodeTask2);


    }

    @Override
    public void remove(int index) {
        if (index < 0 || index >= lenght()) {
            throw new NullPointerException("Данные равны null");
        }
        NodeTask2<T> nodeTask2 = searchNode(index);
//        NodeTask2<T> nodeTask2 = this.head;
//        for (int i = 0; i < index; i++) {
//            nodeTask2 = nodeTask2.next;
//        }
        if (nodeTask2 == this.head) {
            this.head = this.head.getNext();
            this.head.setPrev(null);

        } else if (nodeTask2 == this.last) {
            nodeTask2.getPrev().setNext(null);
            this.last = this.last.getPrev();

        } else {
            nodeTask2.getPrev().setNext(nodeTask2.getNext());
            nodeTask2.getNext().setPrev(nodeTask2.getPrev());
        }


    }

    @Override
    public void remove(T e) {
        NodeTask2<T> tNodeTask2 = new NodeTask2<>(e);


        NodeTask2<T> nodeTask2 = this.head;
        for (int i = 0; i < lenght(); i++) {
            if (i == 0) {
                nodeTask2 = this.head;
            } else {
                nodeTask2 = nodeTask2.next;
            }
            if (nodeTask2.getData().equals(tNodeTask2.getData())) {
                break;
            }
        }

        if (nodeTask2 == this.head) {
            this.head = this.head.getNext();
            this.head.setPrev(null);

        } else if (nodeTask2 == this.last) {
            nodeTask2.getPrev().setNext(null);
            this.last = this.last.getPrev();

        } else {
            nodeTask2.getPrev().setNext(nodeTask2.getNext());
            nodeTask2.getNext().setPrev(nodeTask2.getPrev());
        }

    }

    @Override
    public int lenght() {
        NodeTask2<T> nodeTask2 = this.head;
        int count = 0;
        while (nodeTask2 != null) {
            count++;
            nodeTask2 = nodeTask2.getNext();
        }
        return count;
    }
}
