package s10;

import java.util.ListIterator;

public interface List<T> {
    T get(int i);
    void put(T e);
    void put(int i, T e);
    void remove(int i);
    void remove(T e);
    int lenght();

}
