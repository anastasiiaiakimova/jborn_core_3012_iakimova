package s10;

public class QueueImpl<T> implements Queue<T> {
    NodeTask3<T> head;
    NodeTask3<T> last;
    private int size;

    public QueueImpl(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    @Override
    //добавить элемен в конец очереди
    public boolean add(T e) {
        if (e != null) {
            NodeTask3<T> nodeTask3 = new NodeTask3<>(e);
            if (this.head == null) {
                this.head = nodeTask3;
                this.last = nodeTask3;
                return true;
            } else {
                this.last.next = nodeTask3;
                this.last = nodeTask3;
                return true;
            }
        } else {
            return false;
        }
    }

    @Override
    //возвращает с удалением верхний элемент очереди
    public T poll() {
        if (this.head == null) {
            return null;
        } else {
            NodeTask3<T> nodeTask3 = this.head;
            this.head = this.head.next;
            nodeTask3.next = null;
            return nodeTask3.element;
        }

    }

    @Override
    //возвращает без удаления  верхний элемент очереди
    public T peek() {
        if (this.head == null) {
            return null;
        } else {
            return this.head.element;
        }
    }
}
