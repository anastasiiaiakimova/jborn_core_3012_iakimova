package s10;

public class NodeTask2<T> {
    NodeTask2<T> next;
    NodeTask2<T> prev;
    T data;

    public NodeTask2(T data) {
        this.data = data;
    }

    public void setNext(NodeTask2<T> next) {
        this.next = next;
    }

    public void setPrev(NodeTask2<T> prev) {
        this.prev = prev;
    }

    public void setData(T data) {
        this.data = data;
    }

    public NodeTask2<T> getNext() {
        return this.next;
    }

    public NodeTask2<T> getPrev() {
        return this.prev;
    }

    public T getData() {
        return this.data;
    }
}



