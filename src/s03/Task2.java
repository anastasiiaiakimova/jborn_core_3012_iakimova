package s03;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int numberReg = requestNumber();
        for (int i = 0; i < numberReg; i++) {
            System.out.print((int) (Math.random() * 10));
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
