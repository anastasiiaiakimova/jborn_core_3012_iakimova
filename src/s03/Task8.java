package s03;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String strDef = requestString();
        char[] result = strDef.toCharArray();
        System.out.println("Строка: " + strDef);

        System.out.print("Результат инверсии: ");
        for (int i = result.length - 1; i >= 0; i--) {
            System.out.print(result[i]);
        }

    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
