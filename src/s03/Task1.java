package s03;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        System.out.println("Введите название государства: ");
        String nameCountry = requestNumber();
        System.out.println("Введите название столицы: ");
        String nameCapital = requestNumber();
        System.out.println("Столица государства " + nameCountry + " — город " + nameCapital);
    }

    static String requestNumber() {
        Scanner scanner = new Scanner(System.in);
        //System.out.println("Введите ");
        String s = scanner.nextLine();
        return s;
    }

}
