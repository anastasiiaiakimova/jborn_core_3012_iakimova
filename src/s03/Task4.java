package s03;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Введите сторону 1:");
        int lenA = requestNumber();
        System.out.println("Введите сторону 2:");
        int lenB = requestNumber();
        // StringBuilder defaultStr = new StringBuilder("*");
        StringBuilder sideA = funcSideA(lenA);
        StringBuilder sideB = funcSideB(lenA);

        for (int i = 0; i < lenB; i++) {
            if (i == 0 | i == lenB - 1) {
                System.out.println(sideA);
            } else {
                System.out.println(sideB);
            }
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        //System.out.println("Введите число:");
        return scanner.nextInt();
    }

    static StringBuilder funcSideA(int lenA) {
        StringBuilder s1 = new StringBuilder("*");
        for (int i = 0; i < lenA - 1; i++) {
            s1 = s1.append("*");
        }
        return (s1);
    }

    static StringBuilder funcSideB(int lenA) {
        StringBuilder s2 = new StringBuilder("*");
        for (int i = 0; i < lenA - 1; i++) {
            if (i < (lenA - 2)) {
                s2 = s2.append(" ");
            } else {
                s2 = s2.append("*");
            }
        }
        return (s2);
    }
}
