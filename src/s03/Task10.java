package s03;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        String strDef = requestStr();

        boolean error = (strDef.contains("жы") || strDef.contains("шы") || strDef.contains("ЖЫ")
                || strDef.contains("ШЫ") || strDef.contains("Жы") || strDef.contains("Шы"));

        boolean noError = (strDef.contains("жи") || strDef.contains("ши") || strDef.contains("ЖИ")
                || strDef.contains("ШИ") || strDef.contains("Жи") || strDef.contains("Ши"));

        if (error) {
            System.out.println("Неправильно! Жи/Ши пиши с буквой 'и'");
        } else if (noError) {
            System.out.println("Ошибок нет!");
        } else {
            System.out.println("Нет жи/ши");
        }
    }

    static String requestStr() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку: ");
        return scanner.nextLine();
    }
}
