package s03;

import java.util.Scanner;


public class Task3 {
    public static void main(String[] args) {
        StringBuilder str = requestNumber();
        int l = str.length();
        //System.out.println("Numbers = " + l);
        for (int i = 0; i < l; i++) {
            System.out.println();
            for (int j = 0; j <= i; j++) {
                System.out.print(str.charAt(i));
            }
        }
    }

    static StringBuilder requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово:");
        StringBuilder s = new StringBuilder(scanner.nextLine());
        return s;
    }
}
