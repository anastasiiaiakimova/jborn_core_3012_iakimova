package s03;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int n = requestNumber();
        StringBuilder arrString = new StringBuilder();

        for (int i = 0; i < n; i++) {
            System.out.print("Введите строку " + (i + 1) + " : ");

            arrString = arrString.append(requestStrBuild());
            if (i != (n - 1)) {
                arrString = arrString.append(", ");
            }
        }
        System.out.print("Результат: " + arrString);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите размер массива:");
        return scanner.nextInt();
    }

    static StringBuilder requestStrBuild() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder s = new StringBuilder(scanner.nextLine());
        return s;
    }
}
