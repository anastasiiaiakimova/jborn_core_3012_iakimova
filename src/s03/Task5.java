package s03;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        String str = requestNumber();
        StringBuilder strBuildNew = new StringBuilder(str).reverse();

        if (str.equals(strBuildNew.toString())) {
            System.out.println("Строка " + str + " является палиндромом");
        } else {
            System.out.println("Строка " + str + " не является палиндромом");
        }
    }

    static String requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }

}
