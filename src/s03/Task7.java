package s03;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        System.out.println("Введите строку:");
        StringBuilder strDef = requestString();
        for (int i = 0; i < strDef.length(); i++) {
            if (Character.isUpperCase(strDef.charAt(i))) {
                strDef.setCharAt(i, Character.toLowerCase(strDef.charAt(i)));
            } else {
                strDef.setCharAt(i, Character.toUpperCase(strDef.charAt(i)));
            }
        }
        System.out.println("Измененная строка:" + strDef);

    }

    static StringBuilder requestString() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder s = new StringBuilder(scanner.nextLine());
        return s;
    }
}
