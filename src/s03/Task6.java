package s03;

import java.io.IOException;
import java.util.Scanner;


public class Task6 {
    public static void main(String[] args) throws IOException {
        int counter = 0;
        System.out.println("Введите строку:");
        String str = requestString();
        System.out.println("Введите букву:");
        String charC = requestString();

        for (int i = 0; i < str.length(); i++) {
            char str2 = (str.charAt(i));
            if (charC.charAt(0) == str2) {
                counter++;
            }
        }
        str = str.replaceAll(charC, charC.toUpperCase());
        System.out.println("Количество вхождений " + charC + " в строку: " + counter);
        System.out.println("Преобразованная строка: " + str);
    }


    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

}
