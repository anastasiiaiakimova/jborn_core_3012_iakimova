package s14;

import java.util.Comparator;

public class LtrComparator implements Comparator<SearchRunnable> {

    @Override
    public int compare(SearchRunnable o1, SearchRunnable o2) {
        return Integer.compare(o1.getCount(), o2.getCount());
    }
}
